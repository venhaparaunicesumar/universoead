<link href='https://fonts.googleapis.com/css?family=Mulish' rel='stylesheet'>
<link rel="stylesheet" type="text/css" href="assets/lib/owl/owl.carousel.min.css">
<style type="text/css">
   .cursos_destaques{
   font-family: 'Mulish'; 
   }
   .cursos_destaques .card-text img{
   max-width: 72px;
   }
   .price-new{
   font-family: 'Mulish';
   font-style: normal;
   font-weight: 700;
   font-size: 14px;
   line-height: 17px;
   letter-spacing: 0.1px;
   color: #00538A;
   }
   .price-old{
   font-family: 'Mulish';
   font-style: normal;
   font-weight: 400;
   font-size: 10px;
   line-height: 17px;
   letter-spacing: 0.1px;
   color: #393939;
   }
   .btn-default-cursos_destaques{
   background: linear-gradient(270deg, #00538A 12.09%, #3BB6C6 145.18%);
   color: white!important;
   }
   .offcanvas{
   background-color: #00538A!important;
   }
   .cursos_destaques .btn-close{
   filter: invert(100%) sepia(19%) saturate(3476%) hue-rotate(190deg) brightness(103%) contrast(110%);
   }
</style>
<div class="cursos_destaques py-5">
   <div class="container">
      <div class="row">
         <div class="col-12 col-sm-4 mb-4 mb-sm-0">
            <button class="text-white" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight" style="background: #2C97DE; border-radius: 10px; border: 1px; width: 100%; height: 50px">
            <img src="assets/img/mi_filter.svg"> Filtro
            </button>
            <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
               <div class="offcanvas-header">
                  <h5 class="offcanvas-title text-white" id="offcanvasRightLabel"><img src="assets/img/search-icon.svg" style="filter: invert(100%) sepia(19%) saturate(3476%) hue-rotate(190deg) brightness(103%) contrast(110%);"> 150 Resultados</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
               </div>
               <div class="offcanvas-body">
                  <div class="accordion accordion-flush" id="accordionFlushExample">
                     <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingOne">
                           <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                           Qual produto você quer?
                           </button>
                        </h2>
                        <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                           <div class="accordion-body">
                              <div class="form-check mb-2">
                                 <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault1">
                                 <label class="form-check-label" for="flexCheckDefault1">
                                 Pós
                                 </label>
                              </div>
                              <div class="form-check mb-2">
                                 <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault2">
                                 <label class="form-check-label" for="flexCheckDefault2">
                                 Profissionalizante
                                 </label>
                              </div>
                              <div class="form-check mb-2">
                                 <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault3">
                                 <label class="form-check-label" for="flexCheckDefault3">
                                 Ensino Técnico
                                 </label>
                              </div>
                              <div class="form-check mb-2">
                                 <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault4">
                                 <label class="form-check-label" for="flexCheckDefault4">
                                 Curso Livre
                                 </label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingTwo">
                           <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                           Qual jornada você quer?
                           </button>
                        </h2>
                        <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                           <div class="accordion-body">
                               <div class="form-check mb-2">
                                 <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault5">
                                 <label class="form-check-label" for="flexCheckDefault5">
                                 Rápida EAD
                                 </label>
                              </div>
                              <div class="form-check mb-2">
                                 <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault6">
                                 <label class="form-check-label" for="flexCheckDefault6">
                                 Microcertificação EAD
                                 </label>
                              </div>
                              <div class="form-check mb-2">
                                 <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault7">
                                 <label class="form-check-label" for="flexCheckDefault7">
                                 MBAs e Premium EAD
                                 </label>
                              </div>
                              <div class="form-check mb-2">
                                 <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault8">
                                 <label class="form-check-label" for="flexCheckDefault8">
                                 Híbridos Premium
                                 </label>
                              </div>
                              <div class="form-check mb-2">
                                 <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault9">
                                 <label class="form-check-label" for="flexCheckDefault9">
                                 Ful Presenciais
                                 </label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree">
                           <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                           Qual área?
                           </button>
                        </h2>
                        <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                           <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the third item's accordion body. Nothing more exciting happening here in terms of content, but just filling up the space to make it look, at least at first glance, a bit more representative of how this would look in a real-world application.</div>
                        </div>
                     </div>
                     <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree">
                           <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                           Classificações
                           </button>
                        </h2>
                        <div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
                           <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the third item's accordion body. Nothing more exciting happening here in terms of content, but just filling up the space to make it look, at least at first glance, a bit more representative of how this would look in a real-world application.</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-12 col-sm-8">
            <div class="input-group mb-3">
               <input type="text" class="form-control" placeholder="O que você quer aprender?" aria-label="O que você quer aprender?" aria-describedby="basic-addon2" style="height: 50px">
               <!-- <span class="input-group-text" id="basic-addon2"></span> -->
               <button class="btn btn-outline-secondary" type="button" id="button-addon2"><img src="assets/img/search-icon.svg"></button>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="cursos_destaques cursos_destaques-carousel owl-carousel owl-theme py-5">
            <div class="item">
               <div class="card border-0" >
                  <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                  <div class="card-body">
                     <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                     <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                     <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                     <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                     <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                  </div>
               </div>
            </div>
            <div class="item">
               <div class="card border-0" >
                  <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                  <div class="card-body">
                     <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                     <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                     <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                     <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                     <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                  </div>
               </div>
            </div>
            <div class="item">
               <div class="card border-0" >
                  <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                  <div class="card-body">
                     <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                     <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                     <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                     <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                     <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                  </div>
               </div>
            </div>
            <div class="item">
               <div class="card border-0" >
                  <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                  <div class="card-body">
                     <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                     <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                     <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                     <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                     <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                  </div>
               </div>
            </div>
            <div class="item">
               <div class="card border-0" >
                  <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                  <div class="card-body">
                     <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                     <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                     <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                     <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                     <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>