<style type="text/css">
   .depoimentos{
   background-color: #00538A;
   }
   .depoimentos h1{
   font-family: 'Roboto';
   font-style: normal;
   font-weight: 700;
   /*font-size: 25px;*/
   line-height: 140.62%;
   color: #FFFFFF;
   }
   .depoimentos .card{
   background-color: transparent;
   }
   .depoimentos .card h5 img{
   max-width: 60px;
   border: 1px solid;
   border-radius: 100px;
   background-color: white;
   margin-right: 15px;
   }
   .depoimentos .card-text{
   /*font-family: 'Montserrat';*/
   font-style: normal;
   font-weight: 400;
   font-size: 15px;
   line-height: 30px;
   text-align: justify;
   letter-spacing: -0.015em;
   color: #FFFFFF;
   }
   .depoimentos .depoimento_video:hover{
      opacity: 0.5;
   }
   .depoimento_thumb{
       /*background: url(http://i3.ytimg.com/vi/0_AR7giN0CU/hqdefault.jpg);*/
       cursor: pointer; 
       object-fit: cover;
       display: block;
       margin: 0 auto;
       background-repeat: no-repeat;
       background-size: 100% auto;
       background-position: center top;
       background-attachment: fixed;
       /*height: 160px;*/
       border-radius: 10px;
   }
</style>
<div class="depoimentos">
   <div class="container py-5">
      <div class="row">
         <div class="col-md-12">
            <h1 class="text-center">O que os alunos dizem</h1>
         </div>
      </div>
      <div class="wrapper-with-margin">
         <div class="depoimentos-carousel owl-carousel owl-theme pt-5">
            <div class="item">
               <div class="card d-flex justify-content-center border-0" data-videourl="https://www.youtube.com/embed/WPD10KgvSQo">
                  <h5 class="card-title text-center text-white d-flex justify-content-start align-items-center mb-4
                  "><img src="assets/img/user.svg">Nome</h5>
                  <div class="depoimento_video" style="position: relative;">
                     <img class="depoimento_thumb" src="https://i3.ytimg.com/vi/WPD10KgvSQo/maxresdefault.jpg"/> 
                     <img src="assets/img/player-video.svg" style="width: 50px; position: absolute; top: 50%; transform: translateY(-50%) translateX(-50%); left: 50%;">
                  </div>
                     
                  <div class="card-body">
                     <p class="card-text text-white">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                     </p>
                  </div>
               </div>
            </div>

            <div class="item">
               <div class="card d-flex justify-content-center border-0" data-videourl="https://www.youtube.com/embed/2VT2v1wfw-o">
                  <h5 class="card-title text-center text-white d-flex justify-content-start align-items-center mb-4
                  "><img src="assets/img/user.svg">Nome</h5>
                  <div class="depoimento_video" style="position: relative;">
                     <img class="depoimento_thumb" src="https://i3.ytimg.com/vi/2VT2v1wfw-o/maxresdefault.jpg"/>
                     <img src="assets/img/player-video.svg" style="width: 50px; position: absolute; top: 50%; transform: translateY(-50%) translateX(-50%); left: 50%;"> 
                  </div>
                  <div class="card-body">
                     <p class="card-text text-white">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                     </p>
                  </div>
               </div>
            </div>

            <div class="item">
               <div class="card d-flex justify-content-center border-0" data-videourl="https://www.youtube.com/embed/Nb9uJL4eAwU">
                  <h5 class="card-title text-center text-white d-flex justify-content-start align-items-center mb-4
                  "><img src="assets/img/user.svg">Nome</h5>
                  <div class="depoimento_video" style="position: relative;">
                     <img class="depoimento_thumb" src="https://i3.ytimg.com/vi/Nb9uJL4eAwU/maxresdefault.jpg">
                     <img src="assets/img/player-video.svg" style="width: 50px; position: absolute; top: 50%; transform: translateY(-50%) translateX(-50%); left: 50%;"> 
                  </div>
                     
                  <div class="card-body">
                     <p class="card-text text-white">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                     </p>
                  </div>
               </div>
            </div>

            <div class="item">
               <div class="card d-flex justify-content-center border-0" data-videourl="https://www.youtube.com/embed/zVrsGcpNVg8">
                  <h5 class="card-title text-center text-white d-flex justify-content-start align-items-center mb-4
                  "><img src="assets/img/user.svg">Nome</h5>
                  <div class="depoimento_video" style="position: relative;">
                     <img class="depoimento_thumb" src="https://i3.ytimg.com/vi/zVrsGcpNVg8/maxresdefault.jpg">
                     <img src="assets/img/player-video.svg" style="width: 50px; position: absolute; top: 50%; transform: translateY(-50%) translateX(-50%); left: 50%;"> 
                  </div>
                     
                  <div class="card-body">
                     <p class="card-text text-white">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                     </p>
                  </div>
               </div>
            </div>

         </div>
      </div>
   </div>
</div>