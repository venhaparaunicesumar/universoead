<link href='https://fonts.googleapis.com/css?family=Mulish' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
<link rel="stylesheet" type="text/css" href="assets/lib/owl/owl.carousel.min.css">
<style type="text/css">
   .beneficios{
   font-family: 'Mulish'; 
   }
   .beneficios h1{
   font-family: 'Roboto';
   /*font-size: 25px;*/
   font-weight: 700;
   color: #1D3551;
   }
   .beneficios .card-text{
   font-family: 'Mulish';
   font-style: normal;
   font-weight: 400;
   font-size: 14px;
   line-height: 20px;
   text-align: center;
   letter-spacing: 0.2px;
   color: #1D3551;
   }
   .beneficios .card-title{
   font-family: 'Mulish';
   font-style: normal;
   font-weight: 700;
   font-size: 18px;
   line-height: 24px;
   text-align: center;
   letter-spacing: 0.1px;
   color: #1D3551;
   }
   .owl-carousel .nav-btn{
   height: 47px;
   position: absolute;
   width: 26px;
   cursor: pointer;
   top: 200px !important;
   }
   .owl-carousel .owl-prev.disabled,
   .owl-carousel .owl-next.disabled{
   pointer-events: none;
   opacity: 0.2;
   }
   .owl-carousel .prev-slide{
   background: url(assets/img/nav-icon.svg) no-repeat scroll 0 0;
   left: -33px;
   }
   .owl-carousel .next-slide{
   background: url(assets/img/nav-icon2.svg) no-repeat scroll 0 0;
   right: -33px;
   }
   /*.owl-carousel .prev-slide:hover{
   background-position: 0px -53px;
   }
   .owl-carousel .next-slide:hover{
   background-position: -24px -53px;
   } */ 
   .beneficios .card{
   background: #F2C707;
   border: 1px solid #FFFFFF;
   border-radius: 10px;
   }
   .beneficios .card img{
   max-width: 80px;
   }
   @media only screen and (max-width: 600px) {
   .beneficios .card img{
   max-width: 44px;
   }
   }
   .wrapper-with-margin{
   margin:0px 35px;
   }
</style>
<div class="beneficios">
   <div class="container py-5">
      <div class="row">
         <div class="col-md-12">
            <h1 class="text-center">Quais os Benefícios</h1>
         </div>
      </div>
      <div class="wrapper-with-margin">
         <div class="beneficios-carousel owl-carousel owl-theme pt-5">
            <div class="item">
               <div class="card d-flex justify-content-center py-5">
                  <img class="card-img-top mx-auto" src="assets/img/certificate.svg" alt="Card image cap">
                  <div class="card-body">
                     <h5 class="card-title text-center">Certificado em 8 meses</h5>
                     <p class="card-text">
                        Eos tota dicunt democritum no. Has natum gubergren ne. Est viris soleat sadipscing cu. Legere epicuri   insolens eu ne.
                     </p>
                  </div>
               </div>
            </div>
            <div class="item">
               <div class="card d-flex justify-content-center py-5">
                  <img class="card-img-top mx-auto" src="assets/img/certificate.svg" alt="Card image cap">
                  <div class="card-body">
                     <h5 class="card-title text-center">Certificado em 8 meses</h5>
                     <p class="card-text">
                        Eos tota dicunt democritum no. Has natum gubergren ne. Est viris soleat sadipscing cu. Legere epicuri   insolens eu ne.
                     </p>
                  </div>
               </div>
            </div>
            <div class="item">
               <div class="card d-flex justify-content-center py-5">
                  <img class="card-img-top mx-auto" src="assets/img/certificate.svg" alt="Card image cap">
                  <div class="card-body">
                     <h5 class="card-title text-center">Certificado em 8 meses</h5>
                     <p class="card-text">
                        Eos tota dicunt democritum no. Has natum gubergren ne. Est viris soleat sadipscing cu. Legere epicuri   insolens eu ne.
                     </p>
                  </div>
               </div>
            </div>
            <div class="item">
               <div class="card d-flex justify-content-center py-5">
                  <img class="card-img-top mx-auto" src="assets/img/certificate.svg" alt="Card image cap">
                  <div class="card-body">
                     <h5 class="card-title text-center">Certificado em 8 meses</h5>
                     <p class="card-text">
                        Eos tota dicunt democritum no. Has natum gubergren ne. Est viris soleat sadipscing cu. Legere epicuri   insolens eu ne.
                     </p>
                  </div>
               </div>
            </div>
            <div class="item">
               <div class="card d-flex justify-content-center py-5">
                  <img class="card-img-top mx-auto" src="assets/img/certificate.svg" alt="Card image cap">
                  <div class="card-body">
                     <h5 class="card-title text-center">Certificado em 8 meses</h5>
                     <p class="card-text">
                        Eos tota dicunt democritum no. Has natum gubergren ne. Est viris soleat sadipscing cu. Legere epicuri   insolens eu ne.
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>