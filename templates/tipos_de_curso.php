<style type="text/css">
   .tipos_de_curso{
   background: linear-gradient(270deg, #00538A 12.09%, #3BB6C6 145.18%);
   }
   .tipos_de_curso .accordion{
   background-color: rgba(0, 0, 0, 0);
   --bs-accordion-bg: transparent;
   }
   .tipos_de_curso .accordion-button:not(.collapsed) {
   color: #FFF;
   background-color: rgba(0, 0, 0, 0.1);
   box-shadow: inset 0 calc(var(--bs-accordion-border-width) * -1) 0 var(--bs-accordion-border-color);
   }
   .tipos_de_curso .accordion-button::after{
   filter: invert(69%) sepia(12%) saturate(3323%) hue-rotate(119deg) brightness(111%) contrast(70%);
   }
   .tipos_de_curso .accordion-button:not(.collapsed)::after{
   filter: invert(69%) sepia(12%) saturate(3323%) hue-rotate(119deg) brightness(111%) contrast(70%);
   background-image: var(--bs-accordion-btn-icon)!important;
   }
   .tipos_de_curso .accordion-body{
   background-color: white;
   }
   .tipos_de_curso .accordion-button{
   font-family: 'Mulish';
   font-style: normal;
   font-weight: 700;
   font-size: 18px;
   line-height: 24px;
   letter-spacing: 0.1px;
   }
</style>
<div class="tipos_de_curso">
   <div class="container py-5">
      <!-- <div class="row">
         <div class="col-md-12">
         <h1 class="text-center">Tipos de Curso</h1>
         </div>
         </div> -->
      <div class="row">
         <div class="accordion accordion-flush" id="accordionFlushExample">
            <div class="accordion-item">
               <h2 class="accordion-header" id="flush-headingOne">
                  <button class="accordion-button collapsed text-white" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                  Pós-graduação e MBAs
                  </button>
               </h2>
               <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body">
                     <div class="row">
                        <div class="cursos_destaques cursos_destaques-carousel owl-carousel owl-theme py-5">
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="accordion-item">
               <h2 class="accordion-header" id="flush-headingTwo">
                  <button class="accordion-button collapsed text-white" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                  Profissionalizantes
                  </button>
               </h2>
               <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body">
                     <div class="row">
                        <div class="cursos_destaques cursos_destaques-carousel owl-carousel owl-theme py-5">
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="accordion-item">
               <h2 class="accordion-header" id="flush-headingThree">
                  <button class="accordion-button collapsed text-white" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                  Ensino Técnico
                  </button>
               </h2>
               <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body">
                     <div class="row">
                        <div class="cursos_destaques cursos_destaques-carousel owl-carousel owl-theme py-5">
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="accordion-item">
               <h2 class="accordion-header" id="flush-headingFour">
                  <button class="accordion-button collapsed text-white" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                  Cursos Livres
                  </button>
               </h2>
               <div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body">
                     <div class="row">
                        <div class="cursos_destaques cursos_destaques-carousel owl-carousel owl-theme py-5">
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                           <div class="item">
                              <div class="card border-0" >
                                 <img src="//www.html.am/images/samples/remarkables_queenstown_new_zealand-300x225.jpg" class="card-img-top" alt="..." style="border-radius: 10px;">
                                 <div class="card-body">
                                    <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Sistema Financeiro e Mercado de Capitais</h5>
                                    <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Este curso apresenta uma visão global do sistema financeiro.</p>
                                    <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                                    <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old">R$ 99,00</span></p>
                                    <a href="#" class="btn btn-default-cursos_destaques">Saiba mais</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>