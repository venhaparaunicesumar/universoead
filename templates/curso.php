<style type="text/css">
   .embed-video{
   border-radius: 10px;
   position: relative; padding-bottom: 56.25%; padding-top: 0px; height: 0; overflow: hidden;
   }
   .embed-video iframe{
   position: absolute; top: 0; left: 0; width: 100%; height: 100%; 
   }
   .embed-video a{
   position: absolute;
   top: 50%;
   transform: translateY(-50%) translateX(-50%);
   left: 50%;
   }
   .curso .add_chart{
   background: #F2C707;
   border-radius: 8px;
   max-width: 320px;
   padding: 17px;
   }
   .curso .accordion{
   font-family: 'Mulish';
   }
   .curso .accordion .accordion-body{
   font-family: 'Mulish';
   font-style: normal;
   font-weight: 400;
   font-size: 14px;
   line-height: 20px;
   letter-spacing: 0.3px;
   color: #737B7D;
   transform: matrix(1, 0, 0, 1, 0, 0);
   }
   .curso .accordion-item {
   border: 1.5px solid #30DABD;
   }
   .curso .accordion-button::after{
   filter: invert(69%) sepia(12%) saturate(3323%) hue-rotate(119deg) brightness(111%) contrast(70%);
   }
   .accordion-button:not(.collapsed)::after{
   filter: invert(69%) sepia(12%) saturate(3323%) hue-rotate(119deg) brightness(111%) contrast(70%);
   background-image: var(--bs-accordion-btn-icon)!important;
   }
   .curso h1{
   font-family: 'Roboto';
   color: #00538A;
   font-weight: 700;
   }
   .curso_container-veja-mais{
   max-width: 300px;
   /*height: 250px;
   border: 1px solid black;
   border-radius: 30px;*/
   display: flex;
   justify-content: center;
   align-items: center;
   }
   .curso_container-veja-mais img{
   width: 100%;
   max-width: 250px;
   }
   .color_2C97DE{
      color: #2C97DE;
   }
   .font-weight_700{
      font-weight: 700;
   }
   .font-family_Mulish{
      font-family: 'Mulish';
   }
   .curso .card-text{
      font-family: 'Mulish';
   }
   .curso .price-new{
      font-style: normal;
      font-weight: 700;
      font-size: 17px;
      line-height: 17px;
      letter-spacing: 0.1px;
      color: #02568C;
   }
   .curso .price-old{
      font-size: 14px;
      font-style: normal;
      line-height: 17px;
      letter-spacing: 0.1px;
   }
   .curso .accordion-button{
      font-family: 'Mulish';
      font-style: normal;
      font-weight: 700;
      line-height: 20px;
      letter-spacing: 0.3px;
   }
   .curso .accordion-button.active{
      color: #2C97DE;
      transform: matrix(1, 0, 0, 1, 0, 0);
   }
</style>
<div class="curso py-5">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#" class="color_2C97DE text-decoration-none font-family_Mulish">Home</a></li>
                  <li class="breadcrumb-item color_2C97DE font-weight_700 font-family_Mulish" aria-current="page">Pós-graduação e MBAs</li>
               </ol>
            </nav>
         </div>
      </div>
      <div class="row">
         <div class="col-12 col-lg-6">
            <div class="col-12 col-md-12 mb-4">
               <div class="embed-video" data-id="0_AR7giN0CU" data-controls="0" width="100%"></div>
            </div>
         </div>
         <div class="col-12 col-lg-4 offset-lg-1">
            <div class="card border-0" >
               <div class="card-body">
                  <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Pós em UX para Mobile</h5>
                  <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Projetos web e móveis com belo design para seus clientes usando ferramentas modernas.</p>
                  <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                  <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old text-decoration-line-through">R$ 99,00</span></p>
                  <a href="#" class="btn btn-default-cursos_destaques add_chart d-block mx-auto">Adicionar ao carrinho</a>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="accordion accordion-flush" id="accordionFlushExample">
            <div class="accordion-item">
               <h2 class="accordion-header" id="flush-headingOne">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                  O que você aprenderá:
                  </button>
               </h2>
               <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body">
                     Crie projetos web e móveis com belo design para seus clientes usando ferramentas modernas usadas pelas principais empresas em 2022.
                  </div>
               </div>
            </div>
            <div class="accordion-item">
               <h2 class="accordion-header" id="flush-headingTwo">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                  Este curso inclui:
                  </button>
               </h2>
               <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body">
                     <ul style="list-style: none;">
                        <li style="background-image: url('assets/img/bx_time.svg'); background-repeat: no-repeat; line-height: 15px; padding-left: 20px; margin-bottom: 15px;">26 horas de vídeo sob demanda</li> 
                        <li style="background-image: url('assets/img/akar-icons_book-close.svg'); background-repeat: no-repeat; line-height: 15px; padding-left: 20px; margin-bottom: 15px;">49 artigos</li>
                        <li style="background-image: url('assets/img/ant-design_cloud-download-outlined.svg'); background-repeat: no-repeat; line-height: 15px; padding-left: 20px; margin-bottom: 15px; background-position: -2px;">58 recursos para download</li>
                        <li style="background-image: url('assets/img/akar-icons_clipboard.svg'); background-repeat: no-repeat; line-height: 15px; padding-left: 20px; margin-bottom: 15px; background-position: -2px;">1 exercício do curso</li>
                        <li style="background-image: url('assets/img/la_award.svg'); background-repeat: no-repeat; line-height: 15px; padding-left: 20px; margin-bottom: 15px; background-position: -2px;">Acesso total vitalício</li>
                        <li style="background-image: url('assets/img/movel.svg'); background-repeat: no-repeat; line-height: 20px; padding-left: 20px; margin-bottom: 15px; background-position: 2px;">Acesso no dispositivo móvel e na TV</li>
                        <li style="background-image: url('assets/img/clarity_certificate-line.svg'); background-repeat: no-repeat; line-height: 15px; padding-left: 20px; margin-bottom: 15px; background-position: 2px;">Certificado de conclusão</li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="accordion-item">
               <h2 class="accordion-header" id="flush-headingThree">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                  Veja os módulos:
                  </button>
               </h2>
               <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the third item's accordion body. Nothing more exciting happening here in terms of content, but just filling up the space to make it look, at least at first glance, a bit more representative of how this would look in a real-world application.</div>
               </div>
            </div>
            <div class="accordion-item">
               <h2 class="accordion-header" id="flush-headingFour">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                  Opinião de quem fez:
                  </button>
               </h2>
               <div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the third item's accordion body. Nothing more exciting happening here in terms of content, but just filling up the space to make it look, at least at first glance, a bit more representative of how this would look in a real-world application.</div>
               </div>
            </div>
         </div>
      </div>
      <div class="row py-5">
         <div class="col-md-12">
            <h1 class="text-center">Veja também</h1>
         </div>
      </div>
      <section class="curso_vejaMais">
         <div class="row mb-4">
            <div class="col-12 col-lg-8 mx-auto d-flex justify-content-center align-items-center flex-wrap">
               <div class="col-4 col-lg-6">
                  <div class="col-12 col-md-12">
                     <div class="curso_container-veja-mais">
                        <img src="assets/img/curso_exemplo.jpg">
                     </div>
                  </div>
               </div>
               <div class="col-8 col-lg-6">
                  <div class="card border-0" >
                     <div class="card-body">
                        <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Pós em UX para Mobile</h5>
                        <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Projetos web e móveis com belo design para seus clientes usando ferramentas modernas.</p>
                        <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                        <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old text-decoration-line-through">R$ 99,00</span></p>
                        <!-- <a href="#" class="btn btn-default-cursos_destaques add_chart d-block mx-auto">Saiba mais</a> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12 col-lg-8 mx-auto d-flex justify-content-center align-items-center flex-wrap">
               <div class="col-4 col-lg-6">
                  <div class="col-12 col-md-12">
                     <div class="curso_container-veja-mais">
                        <img src="assets/img/curso_exemplo.jpg">
                     </div>
                  </div>
               </div>
               <div class="col-8 col-lg-6">
                  <div class="card border-0" >
                     <div class="card-body">
                        <h5 class="card-title" style="font-weight: 700; font-size: 14px; line-height: 17px; letter-spacing: 0.1px; color: #373F41;">Pós em UX para Mobile</h5>
                        <p class="card-text m-0" style="font-weight: 400; font-size: 12px; line-height: 16px; letter-spacing: 0.3px; color: #737B7D; transform: matrix(1, 0, 0, 1, 0, 0);">Projetos web e móveis com belo design para seus clientes usando ferramentas modernas.</p>
                        <p class="card-text m-0"><img src="assets/img/avaliacao.svg"></p>
                        <p class="card-text"> <span class="price-new">R$ 99,00</span> <span class="price-old text-decoration-line-through">R$ 99,00</span></p>
                        <!-- <a href="#" class="btn btn-default-cursos_destaques add_chart d-block mx-auto">Saiba mais</a> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </div>
</div>