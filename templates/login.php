<div class="login">
<section class="container">
      <h1 class="TituloForm">Faça seu cadastro</h1>
        <div class="row bordas">
          <form>
              <div class="mb-3 campoNome">
                  <input type="name" class="form-control colorB" id="exampleInputname" aria-describedby="namelHelp" placeholder="Nome completo">
                  <div class="linhaH"></div>
              </div>
              <div class="mb-3 campoEmail">
                <input type="email" class="form-control colorB" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="E-mail">
                  <div class="linhaH"></div>
              </div>
              <div class="mb-3 campoSenha">
                <input type="password" class="form-control colorB" id="exampleInputPassword1" placeholder="Senha">
                  <div class="linhaH"></div>
              </div>
              <div class="mb-3 form-check checkPoint">
                <input type="checkbox" class="form-check-input notificacoes" id="exampleCheck1">
                <label class="form-check-label notificacoes2" for="exampleCheck1">Receber ofertas especiais e notificações personalizadas.</label>
              </div>
              <button type="submit" class="btn btn-warning Bcadastrar">Cadastre-se</button>
            </form>
            <p class="termo">Termos de Políticas de Privacidade</p>
            <p class="JtemConta">Já tem uma conta?<a href="#" class="FacaLogin"><span class="Flogin ">Faça login</span></a></p>
        </div>
    </section>
</div>
<div>
    <a href="https://wa.me/55(aqui seu numero com ddd | tudo junto)?text=Adorei%20seu%20artigo" target="_blank">
    <i style="margin-top:16px" class="fa fa-whatsapp whats"></i>
</a>
</div>
<style>
:root {
    --AmareloBTN:#F2C707;
    --BrancoFundo:#F4F0F0;
    --AzulTitulo:#1d3551;
    --CinzaTextConta:#747474;
    --PretoText:#373F41;

}

.bordas {
    border-radius: 10px;
    border: 1px solid;
    box-shadow: 0px 4px 4px rgb(0 0 0 / 25%);
    box-sizing: border-box;
    height: 600px;
    background: var(--BrancoFundo);
    margin-bottom: 10%;
}

.campoNome {
    margin-top: 10%;
    margin-left: 5%;
    margin-right: 5%;
}

.linhaH {
    width: 98%;
    border-bottom: 2px solid #000000;
    margin-top: 8px;
    margin-left: 11px;
}

.campoEmail {
    margin-left: 5%;
    margin-right: 5%;
}

.campoSenha {
    margin-left: 5%;
    margin-right: 5%;
}

input#exampleInputname {
    background: var(--BrancoFundo);
    border: none;
}

input#exampleInputEmail1 {
    background: var(--BrancoFundo);
    border: none;
}

input#exampleInputPassword1 {
    background: var(--BrancoFundo);
    border: none;
}

.checkPoint {
    margin-left: 5%;
    margin-right: 5%;
    background: var(--BrancoFundo);
}

.Bcadastrar {
    width: 90%;
    margin-top: 50px;
    margin-left: 5%;
    height: 60px;
    font-size: 20px;
    font-weight: 600;
}

.termo {
    font-size: 14px;
    color: var(--PretoText);
    text-align: center;
}

.JtemConta {
    text-align: center;
    padding-left: 79px;
    color: var(--CinzaTextConta);
}

.Flogin {
    text-decoration: underline;
    margin-left: 5px;
}

.TituloForm {
    color: var(--AzulTitulo);
    text-align: center;
    margin-top: 5%;
    margin-bottom: 10%;
}

.FacaLogin {
    color: black;
}

.notificacoes2 {
    color: var(--PretoText);
} */

@media(max-width:576px) {
    section.container {
        padding: 105px !important;
        width: 95%;
    }
}

.whats {
    position: fixed;
    width: 60px;
    height: 60px;
    bottom: 40px;
    right: 40px;
    background-color: #25d366;
    color: #FFF;
    border-radius: 50px;
    text-align: center;
    font-size: 30px !important;
    box-shadow: 1px 1px 2px #888;
    z-index: 1001;
    padding-top: 1rem;
}


</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">