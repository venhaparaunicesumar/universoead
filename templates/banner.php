<div class="banner">
    <div class="container">
        <div class="row">
            <div class="owl-carousel owl-banners">
                <div class="item"><h1>Banner 1</h1></div>
                <div class="item"><h1>Banner 02</h1></div>
                <div class="item"><h1>Banner 03</h1></div>
                <div class="item"><h1>Banner 04</h1></div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})
</script>