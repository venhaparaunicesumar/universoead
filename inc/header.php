<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Universo EAD</title>
    <link rel="stylesheet" href="assets/style.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Mulish' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" href="assets/lib/owl/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="assets/lib/owl/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="assets/lib/youtube-overlay/youtube-overlay.css" rel="stylesheet">
</head>
<body>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <div class="top1">
                  <a class="logo" href="http://localhost/universoead/"><img src="assets/img/logo.png" alt="Logo"></a>
                </div>
                <div class="top2">		
                  <nav id="nav">
                    <button aria-label="Abrir Menu" id="btn-mobile" aria-haspopup="true" aria-controls="menu" aria-expanded="false"><span id="hamburger"></span></button>
                    <ul id="menu" role="menu">
                      <li><a href="/">Sobre</a></li>
                      <li><a href="/">Produtos</a></li>
                      <li><a href="/">Portfólio</a></li>
                      <li><a href="/">Contato</a></li>
                    </ul>
			            </nav>
                </div>
                <div class="top3">		
                 <div class="cart"><img src="assets/img/cart.png" alt="cart"></div>
              </div>
                </div>
            </div>
        </div>
    </div>
  <script>
    const btnMobile = document.getElementById('btn-mobile');
    function toggleMenu(event) {
    if (event.type === 'touchstart') event.preventDefault();
    const nav = document.getElementById('nav');
    nav.classList.toggle('active');
    const active = nav.classList.contains('active');
    event.currentTarget.setAttribute('aria-expanded', active);
    if (active) {
      event.currentTarget.setAttribute('aria-label', 'Fechar Menu');
    } else {
      event.currentTarget.setAttribute('aria-label', 'Abrir Menu');
    }
    }
    btnMobile.addEventListener('click', toggleMenu);
    btnMobile.addEventListener('touchstart', toggleMenu);
</script>